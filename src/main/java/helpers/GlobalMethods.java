package helpers;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import setup.PagesConstructor;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class GlobalMethods extends PagesConstructor {

    public GlobalMethods(WebDriver driver) {
        super(driver);
    }

    public void openUrl(String url) {
        driver.navigate().to(url);
    }

    public void switchTab(int index) {
        /** Declare string type array list: */
        ArrayList<String> tabs
                /** create instance of class ArrayList: */
                = new ArrayList<>
                /** get current tabs and windows in session: */
                (driver.getWindowHandles());
        /** switch focus to tab/window with {index}: */
        driver.switchTo().window(tabs.get(index));
        System.out.println("Switched to page with URL: " + driver.getCurrentUrl());
    }

    public void clickStaleElements(List<WebElement> elements) {
        int i = 0;
        for (WebElement button : elements) {
            elements.get(i).click();
            i++;
        }
    }

    public void clickElementsWithWaitForStaleness(List<WebElement> elements, By selector) {
        for (int index = 0; index < elements.size(); index++) {
            WebElement element = elements.get(index);
            boolean stale = false;
            try {
                if (new WebDriverWait(driver, Duration.ofSeconds(5)).until(ExpectedConditions.stalenessOf(element))) {
                    stale = true;
                }
                if (stale) {
                    element = driver.findElements(selector).get(index);
                }
            } catch (StaleElementReferenceException | TimeoutException exception) {
                System.out.println("Element is not stale: " + element);
            }
            element.click();
        }
    }

    public void switchToFrame(String frameIdOrName) {
        driver.switchTo().frame(frameIdOrName);
    }

    public WebElement getRelativeElement(WebElement parent, By relative) {
      return parent.findElement(relative);
    }
}
