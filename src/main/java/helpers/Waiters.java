package helpers;


import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import setup.PagesConstructor;

import java.time.Duration;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class Waiters extends PagesConstructor {

    public Waiters(WebDriver driver) {
        super(driver);
    }

    public WebElement waitForStalenessWithRefreshElement(WebElement element, By selector) {
        boolean stale = false;
        try {
            if (new WebDriverWait(driver, Duration.ofSeconds(5)).until(ExpectedConditions.stalenessOf(element))) {
                stale = true;
            }
            if (stale) {
                element = driver.findElement(selector);
            }
        } catch (StaleElementReferenceException | TimeoutException exception) {
            System.out.println("Element is not stale: " + element);
        }
        return element;
    }
}
