package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;

public class WebDriverFactory {

    static String browserFromJenkins = System.getenv("BROWSER");

    public static WebDriver initDriver(Browsers browser) {

        if (browserFromJenkins != null) {
            browser = Browsers.valueOf(browserFromJenkins.toUpperCase());
        }
        switch (browser) {
            case EDGE: {
                WebDriverManager.edgedriver().setup();
                return new EdgeDriver();
            }
            case CHROME: {
                WebDriverManager.chromedriver().setup();
//                ChromeOptions chromeOptions = new ChromeOptions();
//                chromeOptions.addArguments("--no-sandbox");
//                chromeOptions.addArguments("disable-gpu");
                return new ChromeDriver();
            }
            case FIREFOX: {
                WebDriverManager.firefoxdriver().setup();
                return new FirefoxDriver();
            }
            case SAFARI: {
                WebDriverManager.safaridriver().setup();
                return new SafariDriver();
            }
        }
        return null;
    }

    public static WebDriver initDriver() {
        String browserName = System.getProperty("browserName", "chrome");
        try {
            return initDriver(Browsers.valueOf(browserName.toUpperCase()));
        } catch (IllegalArgumentException e) {
            System.err.println("This browser is not supported!!!");
            System.exit(-1);
        }
        return null;
    }
}
