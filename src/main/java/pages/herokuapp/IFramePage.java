package pages.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import setup.PagesConstructor;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class IFramePage extends PagesConstructor {

    public IFramePage(WebDriver driver) {
        super(driver);
    }

    private String iFrameID = "mce_0_ifr";

    @FindBy(xpath = "//*[@id='tinymce']")
    WebElement editorText;


    public WebElement getEditorText() {
        return editorText;
    }

    public String getIFrameID() {
        return iFrameID;
    }
}
