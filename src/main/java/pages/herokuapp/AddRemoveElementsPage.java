package pages.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import setup.PagesConstructor;

import java.util.List;

public class AddRemoveElementsPage extends PagesConstructor {

    public AddRemoveElementsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@onclick='addElement()']")
    WebElement addElementButton;

    @FindBy(xpath = "//*[@class='added-manually']")
    List<WebElement> addedElementsButton;

    public void clickAddElement() {
        addElementButton.click();
    }

    public List<WebElement> getAddedElementsButton() {
        return addedElementsButton;
    }

    public void deleteAddedElement() {
        getAddedElementsButton().get(0).click();
    }
}
