package pages.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import setup.PagesConstructor;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class WindowsPage extends PagesConstructor {

    public WindowsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@class='example']/a")
    WebElement clickHereLink;

    public void clickClickHere() {
        clickHereLink.click();
    }
}
