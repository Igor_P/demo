package pages.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.asserts.SoftAssert;
import setup.PagesConstructor;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class ChallengingDomPage extends PagesConstructor {

    SoftAssert softAssert = new SoftAssert();

    List<String> tableTitleNames = Arrays.asList("Lorem", "Ipsum", "Dolor", "Sit", "Amet", "Diceret", "Action");
    List<String> tableValueNames = Arrays.asList("Iuvaret", "Apeirian", "Adipisci", "Definiebas", "Consequuntur", "Phaedrum", "edit delete");

    public ChallengingDomPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[contains(@class, 'button')]")
    List<WebElement> buttons;
    @FindBy(xpath = "//*[@id='content']")
    WebElement content;
    @FindBy(xpath = "//tr/th")
    List<WebElement> tableTitles;
    @FindBy(xpath = "//tr/td")
    List<WebElement> tableRowValues;

    public List<WebElement> getButtons() {
        return buttons;
    }

    public WebElement getContent() {
        return content;
    }

    public void assertTableTitles() {
        int index = 0;
        for (WebElement tableTitle : tableTitles) {
            softAssert.assertEquals(tableTitle.getText(), tableTitleNames.get(index));
            index++;
        }
        softAssert.assertAll();
    }

    public void assertTableRowValues() {
        int index = 0;
        int rowNumber = 0;
        for (WebElement tableValue : tableRowValues) {
//            System.out.println(tableValue.getSize());
//            if (tableValue.getText().contains("edit")) {
//                index = 0;
//                continue;
//            }
//            System.out.println("no edit");
            String value = tableValueNames.get(index) + rowNumber;
            System.out.println(value);
            softAssert.assertEquals(tableValue.getText(), tableValueNames.get(index));
            System.out.println(tableValue.getText());
//            System.out.println(tableValueNames.get(index));
//            System.out.println(index);
//            }
            softAssert.assertAll();
            index = 0;
            rowNumber++;
        }
    }
}
