package pages.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import setup.PagesConstructor;

public class UploadPage extends PagesConstructor {

    String baseDir = System.getProperty("user.dir");

    @FindBy(xpath = "//*[@id='file-upload']")
    private WebElement chooseFileButton;

    @FindBy(xpath = "//*[@id='file-submit']")
    private WebElement uploadSubmitButton;

    public UploadPage(WebDriver driver) {
        super(driver);
    }

    public void uploadFile() {
        chooseFileButton.sendKeys(baseDir + "/src/main/resources/files/Demo.jpg");
        uploadSubmitButton.click();
        //TODO Add assert
    }
}
