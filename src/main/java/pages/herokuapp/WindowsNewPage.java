package pages.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import setup.PagesConstructor;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class WindowsNewPage extends PagesConstructor {
    public WindowsNewPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h3[text()='New Window']")
    WebElement newWindowText;

    public WebElement getNewWindowText() {
        return newWindowText;
    }
}
