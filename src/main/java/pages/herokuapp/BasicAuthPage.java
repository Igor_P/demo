package pages.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import setup.PagesConstructor;

public class BasicAuthPage extends PagesConstructor {

    public BasicAuthPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "content")
    WebElement HeaderContent;

    public WebElement getHeaderContent() {
        return HeaderContent;
    }
}
