package pages.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import setup.PagesConstructor;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class NestedFramesPage extends PagesConstructor {

    private String leftFrameName = "frame-left";

    @FindBy(xpath = "//*[@name='frame-left']")
    WebElement leftFrame;
    @FindBy(tagName = "body")
    WebElement iFrameBody;

    public NestedFramesPage(WebDriver driver) {
        super(driver);
    }


    public String getLeftFrameName() {
        return leftFrameName;
    }

    public WebElement getLeftFrame() {
        return leftFrame;
    }

    public WebElement getiFrameBody() {
        return iFrameBody;
    }
}
