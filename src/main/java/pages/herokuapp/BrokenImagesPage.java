package pages.herokuapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.asserts.SoftAssert;
import setup.PagesConstructor;

import java.util.List;

public class BrokenImagesPage extends PagesConstructor {
    public BrokenImagesPage(WebDriver driver) {
        super(driver);
    }

    SoftAssert softAssert = new SoftAssert();

    @FindBy(xpath = "//*[@class='example']/img")
    List<WebElement> images;

    public List<WebElement> getImages() {
        return images;
    }

    public void assertBrokenImages() {
        for (WebElement image: getImages()) {
            softAssert.assertNotEquals(image.getAttribute("naturalHeight"), "0", " Image is broken, height is 0: " + image.getAttribute("src"));
        }
        softAssert.assertAll("There are broken images!");
    }
}
