package pages.herokuapp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import setup.PagesConstructor;

public class LargeDomPage extends PagesConstructor {
    public LargeDomPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id='sibling-1.1']")
    private WebElement parentElement;
    By relatedElement = By.xpath("./*[@id='sibling-1.3']");

    public WebElement getParentElement() {
        return parentElement;
    }

    public By getRelatedElement() {
        return relatedElement;
    }
}
