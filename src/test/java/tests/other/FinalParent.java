package tests.other;

import org.testng.annotations.Test;

/**
 * Setting the class to "final" will remove possibility to inherit it
 */
public class FinalParent {
    /**
     * Setting the variable to "final" will remove possibility to assign new value to it
     */
    private String voice = "No voice";
    /**
     * Setting the method to "final" will remove possibility to override it
     */
    public void bark() {
        voice = "Woof-woof";
        System.out.println(voice);
    }

    @Test
    public void nonFinalConstantExample() {
        bark();
    }
}
