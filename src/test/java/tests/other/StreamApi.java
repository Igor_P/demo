package tests.other;

public class StreamApi {
//
//    List<String> carBrands = Arrays.asList("Mazda", "Toyota", "Ford");
//    List<String> carBrandsWithDuplicates = List.of("Mazda", "Toyota", "Ford", "Mazda", "Ford");
//
//    @Test
//    public void streamForEach() {
//
//        /**      for (String carBrand : carBrands) {
//         System.out.println(carBrand);
//         }
//         */
//
//        carBrands.stream().forEach(x -> System.out.println(x));
//    }
//
//    @Test
//    public void streamShowDuplicateByValue() {
//        carBrandsWithDuplicates.stream().filter(x -> x.equals("Mazda")).collect(Collectors.toSet()).forEach(x -> System.out.println(x));
//    }
//
//    @Test
//    public void streamRemoveDuplicates() {
//
//
//        /** v1: */
//        List<String> carBrandsWithoutDuplicates = carBrandsWithDuplicates.stream().distinct().collect(Collectors.toList());
//        carBrandsWithoutDuplicates.forEach(x -> System.out.println(x));
//
//        /** v2: */
//        carBrandsWithDuplicates.stream().distinct().collect(Collectors.toList()).forEach(x -> System.out.println(x));
//    }
//
//    @Test
//    public void streamToUpperCase() {
//        carBrands.stream().map(s -> s.toUpperCase()).forEach(x -> System.out.println(x));
//    }
//
//    @Test
//    public void setValuesToStream() {
//        Stream<String> exampleStream = Stream.of("Value1", "Value2");
//        exampleStream.forEach(x -> System.out.println(x));
//    }
//
//    @Test
//    public void countSumOfArrayElements() {
//
//        /** int count = 0;
//         for (int element : new int[]{1, 2, 3}) {
//         count += element;
//         }
//         System.out.println(count);
//         */
//
//        int countSumOfArrayElementsStream = Stream.of(1, 2, 3).reduce(0, (count, element) -> count + element);
//        System.out.println(countSumOfArrayElementsStream);
//    }
}
