package tests.other.Abstraction;

public class Dog extends Animals {

    @Override
    public void voice() {
        System.out.println("Woof");
    }

    @Override
    public void eat() {
        System.out.println("I'm eating dog's food");
    }
}
