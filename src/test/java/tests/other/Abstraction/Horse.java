package tests.other.Abstraction;

public class Horse extends Animals {

    @Override
    public void voice() {
        System.out.println("Neigh");
    }

    @Override
    public void eat() {
        System.out.println("I'm eating horse's food");
    }
}
