package tests.other.Abstraction;

public abstract class Animals {

    public abstract void voice();

    public abstract void eat();

    public void walk() {
        System.out.println("I'm walking");
    }
}
