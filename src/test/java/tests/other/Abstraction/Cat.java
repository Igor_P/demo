package tests.other.Abstraction;

public class Cat extends Animals {

    @Override
    public void voice() {
        System.out.println("Meow");
    }

    @Override
    public void eat() {
        System.out.println("I'm eating cat's food");
    }
}
