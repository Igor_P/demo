package tests.other.Implementation;

public class Main {


    public static void main(String[] args) {
        Cat cat = new Cat();
        Dog dog = new Dog();
        Horse horse = new Horse();

        cat.voice();
        cat.eat();
        cat.walk();
        dog.voice();
        dog.eat();
        dog.walk();
        horse.voice();
        horse.eat();
        horse.walk();
    }
}
