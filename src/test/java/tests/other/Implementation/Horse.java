package tests.other.Implementation;

public class Horse implements Animals {

    @Override
    public void voice() {
        System.out.println("Neigh");
    }

    @Override
    public void eat() {
        System.out.println("I'm eating horse's food");
    }
}
