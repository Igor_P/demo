package tests.other.Implementation;

public interface Animals {

    void voice();

    void eat();

    default void walk() {
        System.out.println("I'm walking");
    }
}
