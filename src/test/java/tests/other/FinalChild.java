package tests.other;

import org.testng.annotations.Test;

public class FinalChild extends FinalParent {

    @Override
    public void bark() {
        System.out.println("Meow O_o");
    }

    @Test
    public void nonFinalMethodExample() {
        bark();
    }
}
