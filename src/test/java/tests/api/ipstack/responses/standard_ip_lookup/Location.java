package tests.api.ipstack.responses.standard_ip_lookup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Location {

    private Integer geoname_id;
    private String capital;
    private ArrayList<Languages> languages;
    private String country_flag;
    private String country_flag_emoji;
    private String country_flag_emoji_unicode;
    private String calling_code;
    private boolean is_eu;

    public int getGeoname_id() {
        return geoname_id;
    }

    public String getCapital() {
        return capital;
    }

    public ArrayList<Languages> getLanguages() {
        return languages;
    }

    public String getCountry_flag() {
        return country_flag;
    }

    public String getCountry_flag_emoji() {
        return country_flag_emoji;
    }

    public String getCountry_flag_emoji_unicode() {
        return country_flag_emoji_unicode;
    }

    public String getCalling_code() {
        return calling_code;
    }

    public boolean isIs_eu() {
        return is_eu;
    }
}
