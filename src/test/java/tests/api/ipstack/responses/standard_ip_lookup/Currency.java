package tests.api.ipstack.responses.standard_ip_lookup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Currency {

    private String code;
    private String name;
    private String plural;
    private String symbol;
    private String symbol_native;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getPlural() {
        return plural;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getSymbol_native() {
        return symbol_native;
    }
}
