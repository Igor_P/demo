package tests.api.ipstack.responses.standard_ip_lookup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Root {

    private String ip;
    private String type;
    private String continent_code;
    private String continent_name;
    private String country_code;
    private String country_name;
    private String region_code;
    private String region_name;
    private String city;
    private String zip;
    private double latitude;
    private double longitude;
    private Location location;
    private TimeZone time_zone;
    private Currency currency;
    private Connection connection;

    public String getIp() {
        return ip;
    }

    public String getType() {
        return type;
    }

    public String getContinent_code() {
        return continent_code;
    }

    public String getContinent_name() {
        return continent_name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public String getCountry_name() {
        return country_name;
    }

    public String getRegion_code() {
        return region_code;
    }

    public String getRegion_name() {
        return region_name;
    }

    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public Location getLocation() {
        return location;
    }

    public TimeZone getTime_zone() {
        return time_zone;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Connection getConnection() {
        return connection;
    }
}
