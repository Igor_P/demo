package tests.api.ipstack.responses.standard_ip_lookup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeZone {

    private String id;
    private Date current_time;
    private int gmt_offset;
    private String code;
    private boolean is_daylight_saving;

    public String getId() {
        return id;
    }

    public Date getCurrent_time() {
        return current_time;
    }

    public int getGmt_offset() {
        return gmt_offset;
    }

    public String getCode() {
        return code;
    }

    public boolean isIs_daylight_saving() {
        return is_daylight_saving;
    }
}
