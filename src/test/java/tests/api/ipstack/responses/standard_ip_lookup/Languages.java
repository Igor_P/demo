package tests.api.ipstack.responses.standard_ip_lookup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Languages {

    private String code;
    private String name;
    @JsonProperty("native")
    private String annotatedNative;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getAnnotatedNative() {
        return annotatedNative;
    }
}
