package tests.api.ipstack.responses.standard_ip_lookup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Connection {

    private int asn;
    private String isp;

    public int getAsn() {
        return asn;
    }

    public String getIsp() {
        return isp;
    }
}
