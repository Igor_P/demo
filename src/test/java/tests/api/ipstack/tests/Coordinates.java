package tests.api.ipstack.tests;

import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.api.ipstack.responses.standard_ip_lookup.Root;

import static io.restassured.RestAssured.given;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class Coordinates {

    private String ip = "91.90.11.152";
    private String accessKey = "82ec359bc907c63681af3e7808645df8";
    private double latitude = 46.477500915527344;
    private double longitude = 30.73259925842285;

    @BeforeClass
    public void beforeClass() {
        RestAssured.baseURI = "http://api.ipstack.com";
    }

    @Test
    public void check_Odessa_IP_Coordinates() {
        Root rootResponse = given().param("access_key", accessKey).log().all().when().get(ip)
                .then().log().all().statusCode(200).extract().as(Root.class);
        Assert.assertEquals(latitude, rootResponse.getLatitude(), 0.01);
        Assert.assertEquals(longitude, rootResponse.getLongitude(), 0.01);
    }
}
