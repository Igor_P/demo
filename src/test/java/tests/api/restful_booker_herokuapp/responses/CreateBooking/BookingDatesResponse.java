package tests.api.restful_booker_herokuapp.responses.CreateBooking;

public class BookingDatesResponse {

    public String checkin;
    public String checkout;

    public String getCheckin() {
        return checkin;
    }

    public String getCheckout() {
        return checkout;
    }
}
