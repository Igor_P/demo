package tests.api.restful_booker_herokuapp.responses.CreateBooking;

public class CreateBookingResponse {

    public Integer bookingid;
    public BookingResponse booking;

    public Integer getBookingid() {
        return bookingid;
    }

    public BookingResponse getBooking() {
        return booking;
    }
}
