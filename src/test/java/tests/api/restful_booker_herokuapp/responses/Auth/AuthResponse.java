package tests.api.restful_booker_herokuapp.responses.Auth;

public class AuthResponse {

    private String token;

    public String getToken() {
        return token.replace("{", "");
    }

    public void setToken(String token) {
        this.token = token;
    }
}
