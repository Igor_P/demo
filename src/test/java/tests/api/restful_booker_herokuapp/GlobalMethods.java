package tests.api.restful_booker_herokuapp;

import io.restassured.http.ContentType;
import org.testng.Assert;
import tests.api.restful_booker_herokuapp.payloads.Auth.AuthPayload;
import tests.api.restful_booker_herokuapp.responses.Auth.AuthResponse;

import static io.restassured.RestAssured.given;

public class GlobalMethods {

    public String getToken(String username, String password) {
        AuthPayload payload = new AuthPayload(username, password);
        AuthResponse authResponse = given().
                body(payload).log().all().
                contentType(ContentType.JSON).
                when().post("auth/").then().log().all().statusCode(200).extract().as(AuthResponse.class);
        Assert.assertNotNull(authResponse.getToken());
        return "token=" + authResponse.getToken();
    }
}
