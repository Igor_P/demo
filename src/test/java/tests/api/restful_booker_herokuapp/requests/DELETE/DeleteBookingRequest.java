package tests.api.restful_booker_herokuapp.requests.DELETE;

public class DeleteBookingRequest {

    private Integer id;

    DeleteBookingRequest(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

}
