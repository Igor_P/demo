package tests.api.restful_booker_herokuapp.requests.POST;

import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import tests.api.restful_booker_herokuapp.payloads.CreateBooking.CreateBookingPayload;

import static io.restassured.RestAssured.given;

public class CreateBookingRequest {

    public static ValidatableResponse createBooking(CreateBookingPayload payload) {

        return given().
                body(payload).
                contentType(ContentType.JSON).log().all().
                when().
                post("booking").
                then().log().all();
    }
}

