package tests.api.restful_booker_herokuapp.tests;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

public class Auth {

    @BeforeClass
    public void beforeClass() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com/";
    }
}
