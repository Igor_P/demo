package tests.api.restful_booker_herokuapp.tests;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.api.restful_booker_herokuapp.GlobalMethods;
import tests.api.restful_booker_herokuapp.payloads.CreateBooking.BookingDatesPayload;
import tests.api.restful_booker_herokuapp.payloads.CreateBooking.CreateBookingPayload;
import tests.api.restful_booker_herokuapp.responses.CreateBooking.CreateBookingResponse;

import static io.restassured.RestAssured.given;

public class DeleteBooking extends GlobalMethods {

    @BeforeClass
    public void beforeClass() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com/";
    }

    @Test
    public void deleteBookingV1() {

        String firstName = "Igor";
        String lastName = "QA";
        int totalPrice = 1000;
        boolean depositPaid = true;
        BookingDatesPayload bookingDatesDTO = new BookingDatesPayload("", "");
        String additionalNeeds = "123";

        CreateBookingPayload payload = new CreateBookingPayload(firstName, lastName, totalPrice, depositPaid, bookingDatesDTO, additionalNeeds);
        CreateBookingResponse response = given().body(payload).contentType(ContentType.JSON)
                .log().all().
                when().
                post("booking").
                then().log().all().statusCode(200).extract().as(CreateBookingResponse.class);
        Assert.assertNotNull(response.getBooking().bookingdates);
        String createdBook = String.valueOf(response.getBookingid());

        ValidatableResponse validatableResponse = given().contentType(ContentType.JSON).
                log().all().header("Cookie", getToken("admin", "password123")).
                when().
                delete("booking/" + createdBook).
                then().
                log().all().
                statusCode(201);
        System.out.println(validatableResponse.extract().body().asPrettyString());
    }
}
