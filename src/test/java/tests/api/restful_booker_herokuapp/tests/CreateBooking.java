package tests.api.restful_booker_herokuapp.tests;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.api.restful_booker_herokuapp.payloads.CreateBooking.BookingDatesPayload;
import tests.api.restful_booker_herokuapp.payloads.CreateBooking.CreateBookingPayload;
import tests.api.restful_booker_herokuapp.requests.POST.CreateBookingRequest;
import tests.api.restful_booker_herokuapp.responses.CreateBooking.CreateBookingResponse;

import java.util.Date;

import static io.restassured.RestAssured.given;

public class CreateBooking {


    @BeforeClass
    public void beforeClass() {
        RestAssured.baseURI = "https://restful-booker.herokuapp.com/";
    }

    @Test
    public void date() {
        Date now = new Date();
        System.out.println(now);
    }

    @Test
    public void createBookingV3() {

        BookingDatesPayload dates = new BookingDatesPayload.Builder()
                .setCheckin("")
                .setCheckout("").build();

        CreateBookingPayload payload = new CreateBookingPayload.Builder()
                .setFirstName("Igor")
                .setLastName("QA")
                .setTotalPrice(1000)
                .setDepositPaid(true)
                .setBookingDates(dates)
                .setAdditionalNeeds("no").build();

        CreateBookingResponse response = CreateBookingRequest.createBooking(payload).extract().as(CreateBookingResponse.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBookingid());
    }

    @Test
    public void createBookingV2() {
        BookingDatesPayload bookingDates = new BookingDatesPayload("", "");
        CreateBookingPayload payload = new CreateBookingPayload("Igor", "QA", 1000,
                true, bookingDates, "111");
        CreateBookingResponse response = CreateBookingRequest.createBooking(payload).extract().as(CreateBookingResponse.class);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getBookingid());
    }

    @Test
    public void createBookingV1() {
        String firstName = "Igor";
        String lastName = "QA";
        int totalPrice = 1000;
        boolean depositPaid = true;
        BookingDatesPayload bookingDates = new BookingDatesPayload("", "");
        String additionalNeeds = "123";

        CreateBookingPayload payload = new CreateBookingPayload(firstName, lastName, totalPrice, depositPaid, bookingDates, additionalNeeds);
        CreateBookingResponse response = given().body(payload).contentType(ContentType.JSON)
                .log().all().
                when().
                post("booking").
                then().log().all().statusCode(200).extract().as(CreateBookingResponse.class);
        Assert.assertNotNull(response.getBooking().bookingdates);

        System.out.println(response.getBooking().firstname);
        Assert.assertNotNull(response.getBooking().bookingdates);
    }
}
