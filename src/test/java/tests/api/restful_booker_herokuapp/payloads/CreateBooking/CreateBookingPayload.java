package tests.api.restful_booker_herokuapp.payloads.CreateBooking;

public class CreateBookingPayload {

    public String firstname;
    public String lastname;
    public int totalprice;
    public boolean depositpaid;
    public BookingDatesPayload bookingdates;
    public String additionalneeds;

    public CreateBookingPayload(String firstname, String lastname, int totalprice, boolean depositpaid, BookingDatesPayload bookingdates, String additionalneeds) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.totalprice = totalprice;
        this.depositpaid = depositpaid;
        this.bookingdates = bookingdates;
        this.additionalneeds = additionalneeds;
    }

    public static class Builder {
        private String firstName, lastName, additionalNeeds;
        private int totalPrice;
        private boolean depositPaid;
        private BookingDatesPayload bookingDates;


        public Builder setFirstName(String firstname) {
            this.firstName = firstname;
            return this;
        }

        public Builder setLastName(String lastname) {
            this.lastName = lastname;
            return this;
        }

        public Builder setTotalPrice(int totalprice) {
            this.totalPrice = totalprice;
            return this;
        }

        public Builder setDepositPaid(boolean depositpaid) {
            this.depositPaid = depositpaid;
            return this;
        }

        public Builder setBookingDates(BookingDatesPayload bookingdates) {
            this.bookingDates = bookingdates;
            return this;
        }

        public Builder setAdditionalNeeds(String additionalneeds) {
            this.additionalNeeds = additionalneeds;
            return this;
        }

        public CreateBookingPayload build() {
            return new CreateBookingPayload(firstName, lastName, totalPrice, depositPaid, bookingDates, additionalNeeds);
        }
    }
}
