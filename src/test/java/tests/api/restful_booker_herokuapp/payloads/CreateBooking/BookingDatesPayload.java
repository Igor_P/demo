package tests.api.restful_booker_herokuapp.payloads.CreateBooking;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BookingDatesPayload {

    String pattern = "yyyy-MM-dd";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    public String checkin;
    public String checkout;

    public BookingDatesPayload(String checkin, String checkout) {
        this.checkin = simpleDateFormat.format(new Date());

        this.checkout = simpleDateFormat.format(new Date());

    }

    public static class Builder {
        private String checkIn, checkOut;

        public Builder setCheckin(String checkIn) {
            this.checkIn = checkIn;
            return this;
        }

        public Builder setCheckout(String checkOut) {
            this.checkOut = checkOut;
            return this;
        }

        public BookingDatesPayload build() {
            return new BookingDatesPayload(checkIn, checkOut);
        }
    }
}
