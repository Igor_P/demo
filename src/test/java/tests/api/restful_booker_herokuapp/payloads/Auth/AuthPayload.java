package tests.api.restful_booker_herokuapp.payloads.Auth;

public class AuthPayload {

    private String username;
    private String password;

    public AuthPayload(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
