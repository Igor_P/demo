package tests.ui;

import helpers.GlobalMethods;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.WindowsNewPage;
import pages.herokuapp.WindowsPage;
import setup.TestsSetup;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class TabsSwitching extends TestsSetup {

    private WindowsPage windowsPage;
    private WindowsNewPage windowsNewPage;
    private GlobalMethods globalMethods;

    @BeforeClass
    public void beforeClass() {
        windowsPage = new WindowsPage(driver);
        windowsNewPage = new WindowsNewPage(driver);
        globalMethods = new GlobalMethods(driver);
        globalMethods.openUrl("https://the-internet.herokuapp.com/windows");
    }

    @Test
    public void switchToSecondTab() {
        windowsPage.clickClickHere();
        globalMethods.switchTab(1);
        windowsNewPage.getNewWindowText().isDisplayed();
    }
}
