package tests.ui;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.TestsSetup;

public class ThreadOne extends TestsSetup {

    @BeforeClass
    public void beforeClass() {
        System.setProperty("webdriver.chrome.driver",
                "./src/main/java/driver/chromedriver");
    }

    @Test
    public void test1() throws InterruptedException {
        System.out.println("Test 1");

        Thread.sleep(3000);
    }

    @Test
    public void test2() throws InterruptedException {
        System.out.println("Test 2");
        Thread.sleep(3000);
    }

    @Test
    public void test3() throws InterruptedException {
        System.out.println("Test 3");
        Thread.sleep(3000);
    }
}
