package tests.ui;

import helpers.GlobalMethods;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.ChallengingDomPage;
import setup.TestsSetup;

public class ChallengingDomTable extends TestsSetup {

    private ChallengingDomPage challengingDomPage;

    @BeforeClass
    public void beforeClass() {
        challengingDomPage = new ChallengingDomPage(driver);
        GlobalMethods globalMethods = new GlobalMethods(driver);
        globalMethods.openUrl("https://the-internet.herokuapp.com/challenging_dom");
    }

    @Test
    public void tableTitles() {
        challengingDomPage.assertTableTitles();
    }

    @Test
    public void tableRows() {
        challengingDomPage.assertTableRowValues();
    }
}
