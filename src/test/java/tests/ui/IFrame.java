package tests.ui;

import helpers.GlobalMethods;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.IFramePage;
import setup.TestsSetup;


public class IFrame extends TestsSetup {

    private IFramePage iFramePage;
    private GlobalMethods globalMethods;

    @BeforeClass
    public void beforeClass() {
        iFramePage = new IFramePage(driver);
        globalMethods = new GlobalMethods(driver);
        globalMethods.openUrl("https://the-internet.herokuapp.com/iframe");
    }

    @Test
    public void checkIFrameText() {
        globalMethods.switchToFrame(iFramePage.getIFrameID());
        iFramePage.getEditorText().isDisplayed();
    }
}
