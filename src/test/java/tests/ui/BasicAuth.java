package tests.ui;

import helpers.GlobalMethods;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.BasicAuthPage;
import setup.TestsSetup;

public class BasicAuth extends TestsSetup {

    private BasicAuthPage basicAuthPage;
    private GlobalMethods globalMethods;

    @BeforeClass
    public void beforeClass() {
        basicAuthPage = new BasicAuthPage(driver);
        globalMethods = new GlobalMethods(driver);
        globalMethods.openUrl("https://admin:admin@the-internet.herokuapp.com/basic_auth");
    }

    @Test
    public void authorized() {
        Assert.assertTrue(basicAuthPage.getHeaderContent().getText().contains("Congratulations! You must have the proper credentials."));
    }
}
