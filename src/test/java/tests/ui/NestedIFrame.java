package tests.ui;

import helpers.GlobalMethods;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.NestedFramesPage;
import setup.TestsSetup;

public class NestedIFrame extends TestsSetup {

    private NestedFramesPage nestedFramesPage;
    private GlobalMethods globalMethods;

    @BeforeClass
    public void beforeClass() {
        nestedFramesPage = new NestedFramesPage(driver);
        globalMethods = new GlobalMethods(driver);
        globalMethods.openUrl("https://the-internet.herokuapp.com/nested_frames");
    }

    @Test
    public void frameText() {
        System.out.println(driver.findElements(By.tagName("frame")).size());
        globalMethods.switchToFrame("frame-top");
        System.out.println(driver.findElements(By.tagName("frame")).size());
        globalMethods.switchToFrame(nestedFramesPage.getLeftFrameName());
        System.out.println(driver.findElements(By.tagName("frame")).size());
        Assert.assertEquals(nestedFramesPage.getiFrameBody().getText(), "LEFT");
    }
}
