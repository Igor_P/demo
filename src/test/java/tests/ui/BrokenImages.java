package tests.ui;

import helpers.GlobalMethods;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.BrokenImagesPage;
import setup.TestsSetup;

public class BrokenImages extends TestsSetup {

    private BrokenImagesPage brokenImagesPage;
    private GlobalMethods globalMethods;

    @BeforeClass
    public void beforeClass() {
        brokenImagesPage = new BrokenImagesPage(driver);
        globalMethods = new GlobalMethods(driver);
        globalMethods.openUrl("https://the-internet.herokuapp.com/broken_images");
    }

    @Test
    public void brokenImages() {
        brokenImagesPage.assertBrokenImages();
    }
}
