package tests.ui;

import helpers.GlobalMethods;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.LargeDomPage;
import setup.TestsSetup;

public class ChildXPath extends TestsSetup {

    private GlobalMethods globalMethods;
    private LargeDomPage largeDomPage;

    @BeforeClass
    public void beforeClass() {
        globalMethods = new GlobalMethods(driver);
        largeDomPage = new LargeDomPage(driver);
        globalMethods.openUrl("https://the-internet.herokuapp.com/large");
    }

    @Test
    public void childXPath() {
        globalMethods.getRelativeElement(largeDomPage.getParentElement(), largeDomPage.getRelatedElement()).getText();
    }
}
