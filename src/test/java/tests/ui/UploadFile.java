package tests.ui;

import helpers.GlobalMethods;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.UploadPage;
import setup.TestsSetup;

public class UploadFile extends TestsSetup {

    private UploadPage uploadPage;
    private GlobalMethods globalMethods;

    @BeforeClass
    public void beforeClass() {
        uploadPage = new UploadPage(driver);
        globalMethods = new GlobalMethods(driver);
        globalMethods.openUrl("https://the-internet.herokuapp.com/upload");
    }

    @Test
    public void uploadFile() {
        uploadPage.uploadFile();
    }
}
