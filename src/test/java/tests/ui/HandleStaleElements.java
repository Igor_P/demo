package tests.ui;

import helpers.GlobalMethods;
import helpers.Waiters;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.ChallengingDomPage;
import setup.TestsSetup;

import java.util.List;

/**
 * Created by Igor Petelsky on Aug, 2022
 */

public class HandleStaleElements extends TestsSetup {

    private ChallengingDomPage challengingDomPage;
    private GlobalMethods globalMethods;
    private Waiters waiters;

    @BeforeClass
    public void beforeClass() {
        globalMethods = new GlobalMethods(driver);
        challengingDomPage = new ChallengingDomPage(driver);
        waiters = new Waiters(driver);
        globalMethods.openUrl("https://the-internet.herokuapp.com/challenging_dom");
    }

    @Test(enabled = false)
    public void reproduceStaleElementException() {
        WebElement content = driver.findElement(By.xpath("//*[@id='content']"));
        driver.navigate().refresh();
        content.isDisplayed();
    }

    @Test
    public void handleStaleElementByTryCatch() {
        String xpath = "//*[@id='content']";
        WebElement content = driver.findElement(By.xpath(xpath));
        driver.navigate().refresh();
        try {
            content.isDisplayed();
        } catch (StaleElementReferenceException e) {
            content = driver.findElement(By.xpath(xpath));
            content.isDisplayed();
        }
    }

    @Test
    public void handleStaleElementByExplicitlyWait() {
        By selector = By.xpath("//*[@id='content']");
        WebElement content = driver.findElement(selector);
        driver.navigate().refresh();
        waiters.waitForStalenessWithRefreshElement(content, selector).isDisplayed();
    }

    @Test
    public void handleStaleElementByPageFactory() {
        challengingDomPage.getContent().isDisplayed();
        driver.navigate().refresh();
        challengingDomPage.getContent().isDisplayed();
    }

    @Test(enabled = false)
    public void reproduceStaleElementsException() {
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(@class, 'button')]"));
        list.get(0).click();
        list.get(1).click();
    }

    @Test
    public void handleStaleElementsByGetIndexPageFactory() {
        challengingDomPage.getButtons().get(0).click();
        challengingDomPage.getButtons().get(1).click();
    }

    @Test
    public void handleStaleElementsByGetIndexAndExplicitlyWait() {
        By selector = By.xpath("//*[contains(@class, 'button')]");
        List<WebElement> list = driver.findElements(selector);
        globalMethods.clickElementsWithWaitForStaleness(list, selector);
    }

    @Test
    public void handleStaleElementsExceptionByGetIndexForEach() {
        globalMethods.clickStaleElements(challengingDomPage.getButtons());
    }
}
