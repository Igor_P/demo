package tests.ui;

import helpers.GlobalMethods;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.herokuapp.AddRemoveElementsPage;
import setup.TestsSetup;

public class AddRemoveElement extends TestsSetup {

    private AddRemoveElementsPage addRemoveElementsPage;
    private GlobalMethods globalMethods;

    @BeforeClass
    public void beforeClass() {
        addRemoveElementsPage = new AddRemoveElementsPage(driver);
        globalMethods = new GlobalMethods(driver);
        globalMethods.openUrl("https://the-internet.herokuapp.com/add_remove_elements/");
    }

    @Test
    public void addAndRemoveElement() {
        Assert.assertEquals(addRemoveElementsPage.getAddedElementsButton().size(), 0);
        addRemoveElementsPage.clickAddElement();
        Assert.assertEquals(addRemoveElementsPage.getAddedElementsButton().size(), 1);
        addRemoveElementsPage.deleteAddedElement();
        Assert.assertEquals(addRemoveElementsPage.getAddedElementsButton().size(), 0);
    }
}
